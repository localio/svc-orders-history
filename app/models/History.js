const orderStatuses = require('@dostolu/constants').orders.status;
const mongoose = require('mongoose');
const { lib: { isConstantInRange } } = require('@dostolu/constants');

// Template schema
const History = new mongoose.Schema({
  /**
   * Creation date
   */
  createdAt: {
    type: Date
  },
  /**
   * Message status
   */
  status: {
    type: Number,
    required: true,
    validate: {
      // Object.values
      validator: (v) => isConstantInRange(v, orderStatuses),
      message: '{VALUE} is not a valid order status'
    },
    index: true
  },
  /**
   * Entity ID
   */
  fk: {
    type: mongoose.Schema.ObjectId,
    required: true,
    index: true
  }
});

// Set creation date
History.pre('save', function (next) { // eslint-disable-line func-names
  if (!this.createdAt) {
    this.createdAt = Date.now();
  }
  next();
});

const HistoryModel = mongoose.model('History', History);

module.exports = HistoryModel;
