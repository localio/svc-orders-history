const HistoryModel = require('../models/History');
const BaseController = require('@dostolu/baseController');

const { schema } = HistoryModel;
const schemaFields = Object.keys(schema.obj);

/**
 * Allowed filters
 * @type {string[]}
 */
const filters = schemaFields.concat(['_id']);
/**
 * Allowed fields
 * @type {string[]}
 */
const fields = schemaFields.concat(['_id']);
/**
 * Allowed includes
 * @type {string[]}
 */
const include = [];
/**
 * Show all fields in short version too
 */
const short = schemaFields.concat(['_id']);
/**
 * Allowed sort fields
*/
const sort = ['createdAt'];

class HistoryController extends BaseController {
  constructor(query) {
    super(query, HistoryModel, {
      filters,
      fields,
      include,
      short,
      sort
    });
  }
}

module.exports = HistoryController;
