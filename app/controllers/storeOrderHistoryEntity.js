const History = require('../models/History');
const log = require('../lib/log')(module);

const storeMessageHistoryEntity = (message) => {
  const history = new History({
    fk: message.payload._id,
    status: message.payload.status
  });
  history.save(err => {
    if (err) {
      log.warn(err);
    }
  });
};

module.exports = storeMessageHistoryEntity;
