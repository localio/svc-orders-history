// Hierarchical node.js configuration with command-line arguments, environment
// variables, and files.
const nconf = require('nconf');
const path = require('path');

/**
 * Check setting existance and throw error if not provided
 * @param {Array} setting Setting name to check
 */
const checkConfig = settings => {
  settings.forEach(setting => {
    if (!nconf.get(setting)) {
      throw new Error(`You must set ${setting} as an environment variable or in config.json!`);
    }
  });
};

nconf
  // 1. Command-line arguments
  .argv()
  // 2. Environment variables
  .env([
    'DB_REDIS_HOST',
    'DB_REDIS_PORT',
    'DB_MONGODB_HOST',
    'DB_MONGODB_USER',
    'DB_MONGODB_PASS',
    'DB_MONGODB_PORT',
    'DB_MONGODB_BASE',
    'PORT',
    'TOPIC_ORDERS',
  ])
  // 3. Config file
  .file({
    file: path.join(__dirname, '../config.json')
  })
  // 4. Defaults
  .defaults({
    DB_REDIS_HOST: 'redis',
    DB_REDIS_PORT: '6379',
    DB_MONGODB_HOST: 'mongodb',
    DB_MONGODB_USER: 'user',
    DB_MONGODB_PASS: 'pass',
    DB_MONGODB_PORT: 27017,
    DB_MONGODB_BASE: 'orders-history',
    isProduction: process.env.NODE_ENV === 'production',
    isTesting: process.env.NODE_ENV === 'testing',
    PORT: 3207,
    TOPIC_ORDERS: 'order',
  });

// Check required settings
checkConfig([
  'DB_MONGODB_HOST',
  'DB_MONGODB_USER',
  'DB_MONGODB_PASS',
  'DB_MONGODB_PORT',
  'DB_MONGODB_BASE',
  'PORT',
  'DB_REDIS_HOST',
  'DB_REDIS_PORT',
  'TOPIC_ORDERS',
]);

/**
 * @typedef {Object}
 *
 * @property {String} DB_MONGODB_HOST
 * @property {String} DB_MONGODB_USER
 * @property {String} DB_MONGODB_PASS
 * @property {String} DB_MONGODB_PORT
 * @property {String} DB_MONGODB_BASE
 *
 * @property {String} DB_REDIS_HOST
 * @property {String} DB_REDIS_PORT
 *
 * @property {Boolean} isProduction
 * @property {Number} PORT
 *
 * @property {String} TOPIC_ORDERS
 */
module.exports = nconf.get();
