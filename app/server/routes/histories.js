const express = require('express');

const router = express.Router();
const HistoryController = require('../../controllers/history');
const validationTransformer = require('@dostolu/validationTransformer');
const { TERM_WRONG_FORMAT, NOT_FOUND_ERROR_NAME } = require('@dostolu/baseController');
const log = require('../../lib/log')(module);

/**
 * Get list
 */
router.get('/', (req, res) => {
  const parameters = { ...req.query, ...req.body };
  const controller = new HistoryController(parameters);
  controller.getList()
    .then((result) => {
      const { data, meta } = result;
      res.status(200).japi.success(data, meta);
    })
    .catch(err => res.status(404).japi.fail(validationTransformer(err)));
});

/**
 * Get by ID
 */
router.get('/:id', (req, res) => {
  const parameters = { ...req.query, ...req.body };
  const controller = new HistoryController(parameters);
  controller.getById(req.params.id)
    .then(history => res.status(200).japi.success(history))
    .catch((err) => {
      switch (err.name) {
        case TERM_WRONG_FORMAT:
          res.status(405).japi.fail(validationTransformer(err));
          break;
        case NOT_FOUND_ERROR_NAME:
          res.status(404).japi.fail(validationTransformer(err));
          break;
        default:
          log.warn(err);
          res.status(500).japi.fail(validationTransformer(err));
          break;
      }
    });
});

module.exports = router;
