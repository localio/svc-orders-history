const express = require('express');

const router = express.Router();
const db = require('../../lib/mongo');
const log = require('../../lib/log')(module);
const pack = require('../../../package.json');
const redis = require('../../lib/redis');

const info = {
  name: pack.name,
  version: pack.version
};

// GET /status
// Check connection to the mongo
router.get('/', (req, res) => {
  const errors = [];
  if (!db.readyState) {
    errors.push('Cannot connect to mongoDB');
  }
  if (!redis.sub.connected) {
    errors.push('Cannot connect to Redis');
  }
  if (!errors.length) {
    res.status(200).japi.success(info);
  } else {
    log.error(errors.join(' '));
    res.status(500).japi.error({
      code: 500,
      message: `${info.name}: Database connection is broken`
    });
  }
});

module.exports = router;
