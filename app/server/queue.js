const Message = require('@dostolu/message');
const config = require('../config');
const { sub } = require('../lib/redis');
const log = require('../lib/log')(module);
const storeOrderHistoryEntity = require('../controllers/storeOrderHistoryEntity');

// subscribe to restaurant channel
sub.subscribe(config.TOPIC_ORDERS);

sub.on('message', (channel, message) => {
  // Track restaurant channel
  log.debug(`sub channel ${channel}: ${message}`);
  switch (channel) {
    case config.TOPIC_ORDERS: {
      // log.info('------------------------------------------------------------');
      // log.info(channel);
      log.info(message);
      // log.info('------------------------------------------------------------');
      const MessengerMessage = new Message().import(message);
      log.info(MessengerMessage);
      switch (MessengerMessage.action) {
        case Message.constants.MESSAGE_ACTION_CREATED:
        case Message.constants.MESSAGE_ACTION_UPDATED:
        case Message.constants.MESSAGE_ACTION_DELETED:
          storeOrderHistoryEntity(MessengerMessage);
          break;
        // no default
      }
      break;
    }
    default:
      log.warn(`Wrong channel: ${channel}`);
  }
});
