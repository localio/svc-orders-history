const { expect } = require('chai');
const mongoose = require('mongoose');
const orderStatuses = require('@dostolu/constants').orders.status;
const HistoryModel = require('../../../app/models/History');
const MongoId = require('mongo-testid');

mongoose.Promise = global.Promise;

describe('History model', () => {
  it('should be invalid if not all required data here in place', (done) => {
    const History = new HistoryModel();
    History.validate((err) => {
      expect(err.errors.status).to.exist;
      done();
    });
  });
  it('should be invalid if wrong constant is passed as a status', (done) => {
    const History = new HistoryModel({
      status: 1
    });
    History.validate((err) => {
      expect(err.errors.status).to.exist;
      done();
    });
  });
  it('should be invalid if no foreigh key has been provided', (done) => {
    const History = new HistoryModel({
      status: orderStatuses.ORDER_STATUS_SENDING_TO_RESTAURANT
    });
    History.validate((err) => {
      expect(err.errors.fk).to.exist;
      done();
    });
  });
  it('should be valid', (done) => {
    const History = new HistoryModel({
      status: orderStatuses.ORDER_STATUS_SENDING_TO_RESTAURANT,
      fk: MongoId()
    });
    History.validate((err) => {
      expect(err).to.be.null;
      done();
    });
  });
});
