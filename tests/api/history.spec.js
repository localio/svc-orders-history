// const chai = require('chai');
// const { expect } = require('chai');
// const chaiHttp = require('chai-http');
// const mongoose = require('mongoose');
// const mongoId = require('mongo-testid');
// const server = require('../../app/server/api');
// const Model = require('../../app/models/History');
// const orderStatuses = require('@dostolu/constants').orders.status;
// const { lib: { isConstantInRange } } = require('@dostolu/constants');

// mongoose.Promise = Promise;

// const fixture = require('./fixture');
// // Feed it to mongodb
// // in before in tests

// function shuffle(a) {
//   for (let i = a.length - 1; i > 0; i--) {
//     const j = Math.floor(Math.random() * (i + 1));
//     [a[i], a[j]] = [a[j], a[i]];
//   }
//   return a;
// }

// chai.use(chaiHttp);

// describe('history', () => {
//   describe('initial state', () => {
//     // #########################################
//     // #
//     // #   BEFORE
//     // #
//     // #########################################
//     describe('/GET history', () => {
//       it('should get the empty history list', (done) => {
//         chai.request(server)
//           .get('/histories')
//           .then((res) => {
//             expect(res.status).to.be.equal(200);
//             expect(res.body.status).to.be.equal('success');
//             expect(res.body.meta.total).to.be.equal(0);
//             expect(res.body.meta.offset).to.be.equal(0);
//             expect(res.body.data).to.be.deep.equal([]);
//             done();
//           });
//       });
//       it('should return 404 when tryitng to get non-existing element', (done) => {
//         chai.request(server)
//           .get(`/histories/${mongoId()}`)
//           .catch((err) => {
//             expect(err.status).to.be.equal(404);
//             expect(err.response.body.status).to.be.equal('fail');
//             done();
//           });
//       });
//     });
//   });
//   describe('initialised state', () => {
//     before(done => {
//       const promises = [];
//       shuffle(fixture.fixture).forEach(item => {
//         promises.push((new Model(item)).save());
//       });
//       Promise.all(promises)
//         .then(() => done());
//     });
//     after(done => {
//       Model.remove({})
//         .then(() => done());
//     });
//     // #########################################
//     // #
//     // #   GET
//     // #
//     // #########################################
//     describe('/GET histories/:id', () => {
//       it('should return 400 when providing wrong ID', (done) => {
//         chai.request(server)
//           .get('/histories/neexistuje')
//           .catch((err) => {
//             expect(err.status).to.be.equal(405);
//             expect(err.response.body.status).to.be.equal('fail');
//             done();
//           });
//       });
//       it('should find history item by id', (done) => {
//         chai.request(server)
//           .get(`/histories/${fixture.randomId}`)
//           .then((res) => {
//             expect(res.status).to.be.equal(200);
//             expect(res.body.status).to.be.equal('success');
//             expect(res.body.data._id).to.be.equal(fixture.randomId);
//             expect(res.body.data.fk).to.be.equal(fixture.order1Id);
//             expect(res.body.data.status)
//               .to.be.equal(orderStatuses.ORDER_STATUS_SUBMITTED_BY_CUSTOMER);
//             done();
//           });
//       });
//     });
//     describe('/GET history', () => {
//       it('should get the history list', (done) => {
//         chai.request(server)
//           .get('/histories')
//           .then((res) => {
//             expect(res.status).to.be.equal(200);
//             expect(res.body.status).to.be.equal('success');
//             expect(res.body.meta.total).to.be.equal(fixture.fixture.length);
//             expect(res.body.meta.offset).to.be.equal(0);
//             done();
//           });
//       });
//       it('should filter list by fk', (done) => {
//         chai.request(server)
//           .get(`/histories?filters[fk]=${fixture.order1Id}`)
//           .then((res) => {
//             expect(res.status).to.be.equal(200);
//             expect(res.body.status).to.be.equal('success');
//             expect(res.body.meta.total).to.be.equal(11);
//             res.body.data.forEach(el => {
//               expect(el.fk).to.be.equal(fixture.order1Id);
//               expect(isConstantInRange(el.status, orderStatuses)).to.be.true;
//               expect(el.createdAt).to.exist;
//               expect(el._id).to.exist;
//             });
//             done();
//           });
//       });
//       it('should filter list by status', (done) => {
//         chai.request(server)
//           .get(`/histories?filters[status]=${orderStatuses.ORDER_STATUS_SUBMITTED_BY_CUSTOMER}`)
//           .then((res) => {
//             expect(res.status).to.be.equal(200);
//             expect(res.body.status).to.be.equal('success');
//             expect(res.body.meta.total).to.be.equal(3);
//             res.body.data.forEach(el => {
//               expect(el.fk).to.exist;
//               expect(el.status).to.be.equal(orderStatuses.ORDER_STATUS_SUBMITTED_BY_CUSTOMER);
//               expect(el.createdAt).to.exist;
//               expect(el._id).to.exist;
//             });
//             done();
//           });
//       });
//       it('should order things', (done) => {
//         chai.request(server)
//           .get(`/histories?filters[fk]=${fixture.order1Id}&sort=-createdAt`)
//           .then(res => {
//             expect(res.status).to.be.equal(200);
//             expect(res.body.status).to.be.equal('success');
//             expect(res.body.meta.total).to.be.equal(11);
//             let date = Date.now();
//             res.body.data.forEach(el => {
//               expect(el.fk).to.be.equal(fixture.order1Id);
//               expect(isConstantInRange(el.status, orderStatuses)).to.be.true;
//               expect(el.createdAt).to.exist;
//               expect(el._id).to.exist;
//               expect(date).to.be.greaterThan(Date.parse(el.createdAt));
//               date = Date.parse(el.createdAt);
//             });
//             done();
//           });
//       });
//     });
//   });
// });
