const moment = require('moment');
const mongoId = require('mongo-testid');
const orderStatuses = require('@dostolu/constants').orders.status;

const order1Id = mongoId('order1');
const order2Id = mongoId('order2');
const order3Id = mongoId('order2');
const randomId = mongoId();
const time = moment().subtract('1 day');

const fixture = [
  // Success
  {
    _id: randomId,
    fk: order1Id,
    createdAt: time.subtract(50, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_SUBMITTED_BY_CUSTOMER
  }, {
    fk: order1Id,
    createdAt: time.subtract(49, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_TO_BE_SENT_TO_RESTAURANT
  }, {
    fk: order1Id,
    createdAt: time.subtract(48, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_SENDING_TO_RESTAURANT
  }, {
    fk: order1Id,
    createdAt: time.subtract(47, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_SENT_TO_RESTAURANT
  }, {
    fk: order1Id,
    createdAt: time.subtract(47, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_RECEIVED_BY_RESTAURANT
  }, {
    fk: order1Id,
    createdAt: time.subtract(46, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_ACCEPTED_BY_RESTAURANT
  }, {
    fk: order1Id,
    createdAt: time.subtract(45, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_COOKING
  }, {
    fk: order1Id,
    createdAt: time.subtract(43, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_DISPATCHED_BY_COURIER
  }, {
    fk: order1Id,
    createdAt: time.subtract(42, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_DELIVERING
  }, {
    fk: order1Id,
    createdAt: time.subtract(41, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_DELIVERED
  },
  // Cancelled by customer
  {
    fk: order2Id,
    createdAt: time.subtract(30, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_SUBMITTED_BY_CUSTOMER
  }, {
    fk: order2Id,
    createdAt: time.subtract(29, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_CANCELLED_BY_CUSTOMER
  },
  // Failed
  {
    fk: order3Id,
    createdAt: time.subtract(10, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_SUBMITTED_BY_CUSTOMER
  }, {
    fk: order3Id,
    createdAt: time.subtract(9, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_SEND_TO_RESTAURANT
  }, {
    fk: order3Id,
    createdAt: time.subtract(8, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_SENDING_TO_RESTAURANT
  }, {
    fk: order3Id,
    createdAt: time.subtract(7, 'minutes').toDate(),
    status: orderStatuses.ORDER_STATUS_CANCELLED_BY_SYSTEM
  }
];

module.exports = {
  order1Id,
  order2Id,
  order3Id,
  randomId,
  fixture
};
