const chai = require('chai');
const { expect } = require('chai');
const chaiHttp = require('chai-http');
const packageJson = require('../../package.json');
const server = require('../../app/server/api');

chai.use(chaiHttp);

describe('status', () => {
  describe('/GET status', () => {
    it('should return proper status', (done) => {
      chai.request(server)
        .get('/status')
        .then((res) => {
          expect(res).to.have.status(200);
          expect(res).to.be.json;
          expect(res).to.have.header('Content-Type', 'application/json; charset=utf-8');
          const json = res.body;
          expect(json.status).to.be.equal('success');
          expect(json.data.name).to.be.equal(packageJson.name);
          expect(json.data.version).to.be.equal(packageJson.version);
          done();
        });
    });
  });
});
