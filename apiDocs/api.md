svc-orders-history API
======================
Provides historical log for all orders in the system


**Version:** 0.0.1

### /status
---
##### ***GET***
**Summary:** System health

**Description:** Check backend health

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | System is up and running |
| 500 | Troubles on the backend side | [Error](#error) |

### /histories
---
##### ***GET***
**Summary:** Entities list

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| fields | query | List of the fields which might be eiter hidden either shown. To set the field which needs to be showm use just field name. To hide field use the field name with the minus. To set fields, related to the included, use dot notation E.g. fields=-slug,-geometry,name,-included.name  | No | string |
| page | query | In case of -1, which is default case for that,  will show all results as a single page  | No | integer |
| page[offset] | query | Pagination offset, number of items to skip | No | integer |
| page[limit] | query | Pagination limit, number of items per page. Default is 10 | No | integer |
| sort | query | Field name to sort,  ASC - if posititive, DESC - if prefixed with the minus sign  | No | string |
| filters | query | Filters to apply. Could be single and multiple. Multiple filters are comma separated, in opeartor will be used. E.g. fields[slug]=kruk,fitelds[_id]=1,2,3,4  | No | string |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | Available history entities list | [Histories](#histories) |
| 500 | Troubles on the backend side | [Error](#error) |

### /histories/{historyId}
---
##### ***GET***
**Summary:** Get history log by ID

**Parameters**

| Name | Located in | Description | Required | Schema |
| ---- | ---------- | ----------- | -------- | ---- |
| historyId | path | History primary key | Yes | string |
| fields | query | List of the fields which might be eiter hidden either shown. To set the field which needs to be showm use just field name. To hide field use the field name with the minus. To set fields, related to the included, use dot notation E.g. fields=-slug,-geometry,name,-included.name  | No | string |

**Responses**

| Code | Description | Schema |
| ---- | ----------- | ------ |
| 200 | History by ID | [History](#history) |
| 404 | Item not found | [Error](#error) |
| 500 | Troubles on the backend side | [Error](#error) |

### Models
---

### Error  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| status | string | Status. Error | No |
| message | string | Short problem description | No |
| code | integer | Status code | No |

### Histories  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| status | string | success | No |
| data | [ [History](#history) ] |  | No |
| meta | [Meta](#meta) |  | No |

### History  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| createdAt | string | Creation date | No |
| status | integer | Order status | No |
| fk | string | Order ID | No |

### Meta  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| total | integer |  | No |
| offset | integer |  | No |
| limit | integer |  | No |

### HistoryId  

| Name | Type | Description | Required |
| ---- | ---- | ----------- | -------- |
| _id | string |  | No |