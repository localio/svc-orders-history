# Messenger history

Logs all actions happening with the message in the system
Keeps message id, date when fired and status
Records history when is able to find appropriate event in the queue.
Purpose - analyze messages behavior

[API doc](apiDocs/api.md)